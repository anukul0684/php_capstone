<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class ServicesControllerTest extends ControllerFunctions
{
    public function testServicesControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/services";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testServicesControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/services";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}