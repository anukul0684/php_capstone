<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class ServicesAtHomeControllerTest extends ControllerFunctions
{
    public function testServicesAtHomeControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/servicesathome";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testServicesAtHomeControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/servicesathome";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}