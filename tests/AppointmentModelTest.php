<?php

use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\AppointmentModel;

/**
 * @file - AppointmentModelTest.php
 * @desc - Child class extended from class TestCase
 * @updated on - 2020-09-09
 */
final class AppointmentModelTest extends TestCase
{
    // a protected property to instantiate the AppointmentModel class
    protected $appointmentModel;

    /**
     * [setup - a function for this test file to create or establish
     * connection with the database and initialize the static proctected
     * property of Model class]
     */
    public function setup()
    {
        $dbh = new PDO('mysql:hostname=localhost;dbname=php_capstone','capstone_user','mycapstone');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->appointmentModel = new AppointmentModel();
    }

    /**
     * [testGetAllAppointmentsReturnsArray - a test function to check if there is
     * a function that gets all the record.]
     */
    public function testGetAllAppointmentsReturnsArray()
    {
        $model = $this->appointmentModel;
        $appointments = $model->all();
        $this->assertIsArray($appointments);
    }

    /**
     * [testGetAllAppointmentsContainsArrayOfAppointments - a test function to
     * check if the all function for all records of the table 
     * has a column name existing]
     */
    public function testGetAllAppointmentsContainsArrayOfAppointments()
    {
        $model = $this->appointmentModel;
        $appointments = $model->all();
        $this->assertArrayHasKey('appointment_time',$appointments[0]);
    }

    /**
     * [testGetOneAppointmentReturnsArrayOfOneAppointment - a test function to 
     * check if a particular record search function exists]
     */
    public function testGetOneAppointmentReturnsArrayOfOneAppointment()
    {
        $model = $this->appointmentModel;
        $appointment = $model->one(1);
        $this->assertIsArray($appointment);
    }
}