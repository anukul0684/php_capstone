<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class CoursesControllerTest extends ControllerFunctions
{
    public function testCoursesControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/courses";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testCoursesControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/courses";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}