<?php

use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\ServiceModel;

/**
 * @file - ServiceModelTest.php
 * @desc - Child class extended from class TestCase
 * @updated on - 2020-09-09
 */
final class ServiceModelTest extends TestCase
{
    // a protected property to instantiate the ServiceModel class
    protected $serviceModel;

    /**
     * [setup - a function for this test file to create or establish
     * connection with the database and initialize the static proctected
     * property of Model class]
     */
    public function setup()
    {
        $dbh = new PDO('mysql:hostname=localhost;dbname=php_capstone','capstone_user','mycapstone');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->serviceModel = new ServiceModel();
    }

    /**
     * [testGetAllServicesReturnsArray - a test function to check if there is
     * a function that gets all the record.]
     */
    public function testGetAllServicesReturnsArray()
    {
        $model = $this->serviceModel;
        $services = $model->all();
        $this->assertIsArray($services);
    }

    /**
     * [testGetAllServicessContainsArrayOfServices -a test function to
     * check if the all function for all records of the table 
     * has a column name existing]
     */
    public function testGetAllServicesContainsArrayOfServices()
    {
        $model = $this->serviceModel;
        $services = $model->all();
        $this->assertArrayHasKey('service_name',$services[0]);
    }

    /**
     * [testGetOneServiceReturnsArrayOfOneService -a test function to 
     * check if a particular record search function exists]
     */
    public function testGetOneServiceReturnsArrayOfOneService()
    {
        $model = $this->serviceModel;
        $service = $model->one(1);
        $this->assertIsArray($service);
    }
}