<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class ProductsControllerTest extends ControllerFunctions
{
    public function testProductsControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/products";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testProductsControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/products";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}