<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class LoginControllerTest extends ControllerFunctions
{
    public function testLoginControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/login";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testLoginControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/login";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}