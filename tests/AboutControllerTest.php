<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class AboutControllerTest extends ControllerFunctions
{
    public function testAboutControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/about";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testAboutControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/about";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}