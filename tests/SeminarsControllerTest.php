<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class SeminarsControllerTest extends ControllerFunctions
{
    public function testSeminarsControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/seminars";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testSeminarsControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/seminars";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}