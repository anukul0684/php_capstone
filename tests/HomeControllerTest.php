<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class HomeControllerTest extends ControllerFunctions
{
    public function testHomeControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/home";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testHomeControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/home";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}