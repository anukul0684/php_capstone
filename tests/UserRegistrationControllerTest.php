<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class UserRegistrationControllerTest extends ControllerFunctions
{
    public function testUserRegistrationControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/user_registration";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testUserRegistrationControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/user_registration";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}