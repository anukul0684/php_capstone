<?php
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/ControllerFunctions.php';

final class ContactUsControllerTest extends ControllerFunctions
{
    public function testContactUsControllerTestsReturnsCorrectResponse()
    {
        $url = "http://capstone1.local/contact";
        $status = $this->getHttpStatus($url);
        $this->assertEquals('200', $status);
    }

    public function testContactUsControllerTestsReturnsCorrectContent()
    {
        $url = "http://capstone1.local/contact";
        $response = $this->getHttpResponse($url); // custom method
        $this->assertContains('<h1><a title="Company Name" href="/">Angel Salon </a></h1>', $response);
    }
}