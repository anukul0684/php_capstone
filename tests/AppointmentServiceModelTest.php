<?php

use PHPUnit\Framework\TestCase;
use App\Models\Model;
use App\Models\AppointmentServiceModel;

/**
 * @file - AppointmentServiceModelTest.php
 * @desc - Child class extended from class TestCase
 * @updated on - 2020-09-09
 */
final class AppointmentServiceModelTest extends TestCase
{
    // a protected property to instantiate the AppointmentServiceModel class
    protected $appointmentServiceModel;

    /**
     * [setup - a function for this test file to create or establish
     * connection with the database and initialize the static proctected
     * property of Model class]
     */
    public function setup()
    {
        $dbh = new PDO('mysql:hostname=localhost;dbname=php_capstone','capstone_user','mycapstone');

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        Model::init($dbh);

        $this->appointmentServiceModel = new AppointmentServiceModel();
    }

    /**
     * [testGetAllAppointmentServicesReturnsArray - a test function to check if there is
     * a function that gets all the record.]
     */
    public function testGetAllAppointmentServicesReturnsArray()
    {
        $model = $this->appointmentServiceModel;
        $appointmentServices = $model->all();
        $this->assertIsArray($appointmentServices);
    }

    /**
     * [testGetAllAppointmentServicesContainsArrayOfAppointmentServices -a test function to
     * check if the all function for all records of the table 
     * has a column name existing]
     */
    public function testGetAllAppointmentServicesContainsArrayOfAppointmentServices()
    {
        $model = $this->appointmentServiceModel;
        $appointmentServices = $model->all();
        $this->assertArrayHasKey('appointment_id',$appointmentServices[0]);
    }

    /**
     * [testGetOneAppointmentServiceReturnsArrayOfOneAppointmentService -a test function to 
     * check if a particular record search function exists]
     */
    public function testGetOneAppointmentServiceReturnsArrayOfOneAppointmentService()
    {
        $model = $this->appointmentServiceModel;
        $appointmentService = $model->one(1);
        $this->assertIsArray($appointmentService);
    }
}