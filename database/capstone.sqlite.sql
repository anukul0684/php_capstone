DROP TABLE IF EXISTS `appointment_services`;
CREATE TABLE `appointment_services` (
  `appointment_services_id` integer NOT NULL,
  `appointment_id` integer NOT NULL,
  `service_id` integer NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `appointment_service_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`appointment_services_id`)
);
INSERT INTO `appointment_services` 
VALUES 
(1,1,5,'2020-09-08 23:55:45',NULL,'Yes');
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE `appointments` (
  `appointment_id` integer NOT NULL,
  `user_id` integer NOT NULL,
  `appointment_date` datetime NOT NULL,
  `appointment_time` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `appointment_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`appointment_id`)
);
INSERT INTO `appointments` 
VALUES 
(1,1,'2020-09-08 23:53:36','2020-09-08 23:53:36','2020-09-08 23:53:36',NULL,'Yes');
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `service_id` integer NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `service_category` char(20) NOT NULL,
  `service_type` char(20) NOT NULL,
  `service_price` decimal(5,2) NOT NULL,
  `service_description` text NOT NULL,
  `service_rating` integer DEFAULT NULL,
  `service_review` text DEFAULT NULL,
  `service_image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `service_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`service_id`)
);
INSERT INTO `services` 
VALUES 
(1,'Fruit Facial','Facial','salon',60.00,'Fruit facial is also a skin polishing facial that scrubs off the dead skin, revives the complexion and lightens the skin complexion. The fruit mask will further tighten the skin and improvises the skin texture and smoothness. The fruit masks also rejuvenated the damaged cells of the skin.',8,'So happy to use the services here.','fruit_facial.jpg','2020-09-08 19:18:53',NULL,1),
(2,'Herbal Facial','Facial','salon',60.00,'Herbal facials are one of the best ways to cleanse the face and promote glowing, healthy skin. Herbs such as comfrey, elder blossoms, chamomile and rosemary will cleanse and soften skin while drawing out toxins and impurities from the pores.',9,'Very professional staff and good service. Very nice products used here. Rejuvinating.','herbal_facial.jpg','2020-09-08 19:22:10',NULL,1),
(3,'Hydro-Oxy Facial','Facial','salon',100.00,'The facial involves a machine that sprays atomized moisturizers onto the skin using a stream of pressurized oxygen. The treatment is supposed to hydrate skin immediately, making the face appear smoother and plumper.',10,'Very professional staff and good service. Very nice products used here.','hydro_oxy_facial.jpg','2020-09-08 19:24:39',NULL,1),
(4,'Anti-ageing Facial','Facial','salon',80.00,'Anti-aging facials use products and techniques designed to slow the aging process, brighten skin, and reduce wrinkles. ... There are a variety of anti-aging facial treatments to select from, including those utilizing vitamin-infused serums, collagen creams, and rejuvenating facial massage techniques.',10,'Very professional staff and good service.','anti_ageing_facial.jpg','2020-09-08 19:44:46',NULL,1),
(5,'Balancing Facial','Facial','salon',70.00,'A soothing, healing facial that delivers essential nutrients to restore the skin and bring it back into balance. Highly effective, certified organic ingredients combined with anti-oxidants help to restore and rebalance all skin types, even the most sensitive.',10,'Very professional staff.','balancing_facial.jpg','2020-09-08 19:47:01',NULL,1),
(6,'Full-hand Waxing','Waxing','salon',30.00,'A full arm wax will include everything from your shoulder downwards, including hands and fingers if required. For those who hate shaving, the underarm wax gets rid of all the hair in your armpits.',10,'Very professional staff.','hand_waxing.jpg','2020-09-08 19:50:07',NULL,1),
(7,'Full-leg Waxing','Waxing','salon',30.00,'A full leg wax includes all of the hair growth from the top of the foot to the top of the thigh.',10,'Very nice place and nice staff.','leg_waxing.jpg','2020-09-08 19:50:29',NULL,1),
(8,'Back Waxing','Waxing','salon',50.00,'You will be applied the hot wax and it will be removed without getting too uncomfortable, so it might be a good pick for those who have sensitive skin or who are afraid of the pain that comes with waxing.',10,'Very nice place','back_waxing.jpg','2020-09-08 19:51:11',NULL,1),
(9,'Full Body Waxing','Waxing','salon',160.00,'The Full Body Wax package consists of a Complete hard Wax, waxing all parts of the legs, waxing the area under the arms and the area above the top lip. This means that requesting for a full body wax implies that all the above areas will be waxed.',10,'Very nice place','body_waxing.jpg','2020-09-08 19:51:52',NULL,1),
(10,'Medium Layers','Hair Cut','salon',60.00,'A medium-cut sees the ends of your hair at your collarbones or your shoulder blades. This type of hairstyle works with all hair types - from straight to wavy, curly, and natural - as well as all hair textures.',10,'These guys are so good.','medium_layers.jpg','2020-09-08 19:53:27',NULL,1),
(11,'Bangs','Hair Cut','salon',60.00,'Bangs (also known as a fringe) are strands or locks of hair that fall over the scalp front hairline to cover the forehead, usually just above the eyebrows, though can range to various lengths.',10,'These guys are awesome in haircuts.','bangs.jpg','2020-09-08 19:55:25',NULL,1),
(12,'Perm','Hair Cut','salon',60.00,'Perms may be applied using thermal or chemical means. In the latter method, chemicals are applied to the hair, which is then wrapped around forms to produce hairstyles. The same process is used for chemical straightening or relaxing, with the hair being flattened instead of curled during the chemical reaction.',10,'Very nice hair cut done.','perm.jpg','2020-09-08 19:59:58',NULL,1),
(13,'The Bob','Hair Cut','salon',60.00,'A bob cut, also known as a bob, is a short- to medium-length haircut, in which the hair is typically cut straight around the head at about jaw-level, often with a fringe (or bangs) at the front. The standard bob is normally cut either between or just below the tips of the ears, and well above the shoulders.',10,'Much experienced staff.','bob.jpg','2020-09-08 20:01:52',NULL,1),
(14,'Blow Dry','Hair Styling','salon',20.00,'A blow-dry is a method of styling the hair while drying it with a hand-held hairdryer. The price of a cut and blow-dry varies widely.',10,'Reasonable pricing.','blowdry.jpg','2020-09-08 20:04:01',NULL,1),
(15,'Curls','Hair Styling','salon',30.00,'20 Best Layered Hairstyles For Curly Hair',10,'Very nice staff.','curls.jpg','2020-09-08 20:04:25',NULL,1),
(16,'Straightening','Hair Styling','salon',40.00,'A hair styling technique used involving the flattening and straightening of hair in order to give it a smooth, streamlined, and sleek appearance.',10,'Highly professional.','straightening.jpg','2020-09-08 20:06:11',NULL,1),
(17,'Hair Color','Hair Styling','salon',30.00,'Hair coloring products using a two-step process (usually occurring simultaneously) which first removes the original color of the hair and then deposits a new color. It is essentially the same process as lightening except a colorant is then bonded to the hair shaft.',10,'Highly professional, got all shades and brands for hair coloring.','hair_color.jpg','2020-09-08 20:06:57',NULL,1),
(18,'Eyebrows','Threading','salon',10.00,'A piece of thread is taken and twisted on itself as it is glided along the brow to shape it. The hair is removed from the follicle, but it does not damage the skin. Before the actual threading happens, you will be given a consultation to decide on shape. The whole process takes around 15 minutes to complete.',10,'Very nice shapes given everytime.','eyebrows.jpg','2020-09-08 20:08:53',NULL,1),
(19,'Upperlips','Threading','salon',10.00,'While waxing is efficient for large sections of skin like the hands and legs, threading is better for small areas like the upper lip. The reason why threading is ideal for all skin types is because the pressure of it can be adjusted accordingly.',10,'Very nicely done','upperlips.jpg','2020-09-08 20:11:01',NULL,1),
(20,'Forehead','Threading','salon',10.00,'Less messy and time consuming than waxing, forehead threading will pick up the finest hairs from the surface of the skin and minimizes the chance for breakouts compared to other hair removal methods! Benefits: - Hair grows back softer and thinner. - No rashes or discoloration of skin.',10,'Very nice','forehead.jpg','2020-09-08 20:11:33',NULL,1),
(21,'Full Face','Threading','salon',40.00,'Full face threading allows unwanted hair removal to be done with precision, leaving fewer strays behind. Not damaging to the skin and suitable for all skin types, full face threading opens the pores, giving skin a mini facial in the process.',10,'Very nice','full_face.jpg','2020-09-08 20:12:40',NULL,1),
(22,'Eye Makeup','Makeup','salon',20.00,'Eye makeup is a type of cosmetics which aims to make the eyes look noticeable and attractive. It is mostly used by females, and by stage performers of all types. Eye makeup is an important part of the fashion and cosmetic industries.',10,'Very happy with the services. So many options available and very knowledgeable for which one to go with the looks','eyemakeup.jpg','2020-09-08 20:13:30',NULL,1),
(23,'Party Ready','Makeup','salon',65.00,'For the party makeup look you choose to do, your outfit is as important as the makeup you add to it. However, whatever you decide to wear, the products in your makeup kit should include basics like as a concealer, foundation, kohl blush as well as a sexy lip colour.',10,'Very happy with the services. They make you party ready as per your outfit and event','party_ready.jpg','2020-09-08 20:15:26',NULL,1),
(24,'Bridal','Makeup','salon',200.00,'Bridal makeup is an essential part of the wedding planning process and designing the perfect look for your wedding day is my number one priority. ... This is the perfect option for anyone who does not typically wear a lot of makeup or someone who wants to look ultra natural with a touch of elegance.',10,'Best place for Bridal look','bridal.jpg','2020-09-08 20:15:49',NULL,1),
(25,'Indian Bride','Makeup','salon',220.00,'If you are an Indian bride who plans to wear a traditional Sari or Lehenga of red, maroon, gold or green colors, you may also want to consider wearing Indian bridal makeup. Traditionally, Indian bridal makeup takes in the whole person and consists of 16 items including the dress.',10,'Best place to get a Indian Bridal look','indian_bridal.jpg','2020-09-08 20:16:37',NULL,1),
(26,'Pedicure','Skin Care','salon',50.00,'A pedicure is a cosmetic treatment of the feet and toenails, analogous to a manicure. Pedicures are done for cosmetic, therapeutic purposes. ... Pedicures include care not only for the toenails; dead skin cells are rubbed off the bottom of the feet using a rough stone (often a pumice stone).',10,'very professional','pedicure.jpg','2020-09-08 20:18:14',NULL,1),
(27,'Manicure','Skin Care','salon',40.00,'A Manicure is a cosmetic beauty treatment for the fingernails and hands. ... A Luxury Manicure will nourish and hydrate your hands and cuticles. This indulgent treatment includes a hand exfoliation and a relaxing hand and arm massage using specially selected products.',10,'Nicely done','manicure.jpg','2020-09-08 20:19:03',NULL,1),
(28,'Head Massage','Skin Care','salon',20.00,'A scalp massage is a head massage designed to relax the mind and encourage circulation. Many times, tension is felt within the head and neck, so scalp massages can be very effective as a stress reducer. Warm oil is massaged throughout the scalp, working to relax tight muscles in the temple and neck regions.',10,'rejuvinating','head_massage.jpg','2020-09-08 20:19:50',NULL,1),
(29,'Full Body Massage','Skin Care','salon',60.00,'In a full-body massage, a therapist will massage the entire body during a therapeutic massage. Sessions typically last a minimum of 50 minutes which allows enough time to work over all the major areas of the body like the back, shoulders, legs, feet, arms, hands, and neck.',10,'Complete relaxation','full_body_massage.jpg','2020-09-08 20:20:34',NULL,1),
(30,'Back Massage','Skin Care','salon',30.00,'Deep Tissue Massage, Sports Massage, Remedial Massage, Relaxation massage, Swedish massage and more. A back massage can provide many benefits, including: Reducing tightness. Relaxing the back muscles.',10,'stimulate circulation and relax stiff muscles, with several types featuring programs designed for upper, lower and full back.','back_massage.jpg','2020-09-08 20:22:40',NULL,1);
DROP TABLE IF EXISTS `user_registration`;
CREATE TABLE `user_registration` (
  `id` integer NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `street_details` varchar(255) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `province_name` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(14) NOT NULL,
  `user_type` char(20) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO `user_registration` 
VALUES 
(1,'Anu','Kulshrestha','Camden','Winnipeg','Manitoba','Canada','R3G 2V8','anu@gmail.com','204-599-8236','customer','$2y$10$UBGABRBj0e9OdG4Mpu8g0exDjVV0O9eqT0ykvSGjwGrYi478qMCjS','2020-09-08 16:14:19','2020-09-08 16:14:19'),(2,'Asha','Sharma','Veresh','Winnipeg','Manitoba','Canada','E2V 5Y6','asha@gmail.com','123-897-8574','customer','$2y$10$G7sUAjwnWf3tcP.bCD1ZK.IE1nk2y7M0RoWlCnNzl3Jaaoq/ei8H6','2020-09-09 02:09:36','2020-09-09 02:09:36');
