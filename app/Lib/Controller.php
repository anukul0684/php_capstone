<?php

namespace App\Lib;

/**
 * @file - Controller.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated - 2020-09-30
 */
abstract class Controller
{
    /**
     * [$view_path initialize with path]
     * @var string
     */
    protected static $view_path = '';

    /**
     * [init initialize with path]
     * @param  [string] $path [path to follow]
     */
    public static function init($path)
    {
        self::$view_path = $path;
    }

    /**
     * [view get the view page of the controller]
     * @param  [string] $view [name of the page]
     * @param  array  $data [all details to send to the page]
     */
    protected function view($view, $data=[])
    {
        extract($data);
        global $flash;
        global $errors; 
        global $post; 
        global $token;
        $errors = $_SESSION['errors'] ?? [];
        $post = $_SESSION['post'] ?? [];
        if (!isset($_SESSION['token'])) {
            $token = bin2hex(random_bytes(32));
            $_SESSION['token'] = $token;
            $_SESSION['token_time'] = time();
        }
        else
        {
            $token = $_SESSION['token'];
        }
        require self::$view_path . "/{$view}.view.php";
    }
}