<?php

/**
 * @file - helpers.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated - 2020-09-18
 */


/**
 * [dump_continue - function for var_dump and continue script]
 * @param  Mixed $var [variable to var_dump]
 */
function dump_continue($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}


/**
 * [dump_die - function for var_dump and die in script]
 * @param  Mixed $var [variable to var_dump]
 */
function dump_die($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die; //kills the script right there
}


/**
 * [e - function to escape output]
 * @param  string $str [data to output on browser]
 * @return string      [data ready for output]
 */
function e($str)
{
    //ENT_QUOTES - if there is any quotes in it convert it to entities.
    //html entities will convert any executable code into entities
    return htmlentities($str, ENT_QUOTES,"UTF-8");
}


/**
 * [flash - a function to carry message across the website]
 * @param  string $type    [description]
 * @param  string $message [description]
 */
function flash($type,$message)
{
    $_SESSION['flash'][] = [$type, $message];
}

/**
 * [prepare_log knit the log details]
 * @return [string] [ready content for log]
 */
function prepare_log()
{
    date_default_timezone_set("Canada/Central");   
    $event = date("Y-m-d H:i:s") . ' | ' . $_SERVER['REQUEST_METHOD'] . ' | ';
    $event = $event . $_SERVER['REQUEST_URI']. ' | ' . http_response_code();
    $event = $event . ' | ' . $_SERVER['HTTP_USER_AGENT'] . ' | ';
    $event = $event . ' Logged.';  
    return $event;
}

/**
 * [log_function called on every page load for logging into file or database]
 * @param  \App\Lib\ILogger $logger [object of class implementing interface ILogger]
 */
function log_function(\App\Lib\ILogger $logger) 
{ 
    $event = prepare_log(); 
    $logger->write($event); 
}
