<?php

namespace App\Lib;

use App\Lib\Controller;

/**
 * @file - Router.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated - 2020-09-18
 */
class Router
{
    private $request='';
    private $method='';
    private $get = [];
    private $post = [];
    private $put_array = [];
    private $delete_array=[];
    
    /**
     * [__construct magic function invoked when class is instantiated]
     * @param string $request [page name]
     * @param string $method  [method]
     */
    public function __construct(string $request,string $method) 
    {
        $this->request=trim($request,'/ ');
        $this->method=$method;
    }

    /**
     * [get - assign to get array the requested page]
     * @param  string   $route    [page name]
     * @param  callable $callable [associative array function]
     */
    public function get(string $route,callable $callable)
    {
        //callable means executable
        $clean_route = trim($route,'/ ');
        $this->get[$clean_route] = $callable;
    }

    /**
     * [get - assign to get array the requested page]
     * @param  string   $route    [page name]
     * @param  callable $callable [associative array function]
     */
    public function post(string $route,callable $callable)
    {
        //callable means executable
        $clean_route = trim($route,'/ ');
        $this->post[$clean_route] = $callable;
    }

    /**
     * [dispatch - implements function]
     */
    public function dispatch()
    {
        if($this->method == 'GET') {
            $this->dispatch_get();
        }
        if($this->method == 'POST') {
            $this->dispatch_post();
        }
    }

    public function dispatch_get()
    {
        try
        {
            foreach ($this->get as $route => $callable) {
                # code...
                $pattern = '/^' . $route . '$/';
                if(preg_match($pattern, $this->request, $matches)) {
                    array_shift($matches);
                    call_user_func_array($callable, $matches);   
                    return;             
                }
            }
        }
        catch(Exception $exception_error)
        {
            flash('error', 'Some error has occurred. Please try again.');
            $data['flash'] = $_SESSION['flash'];
            unset($_SESSION['flash']);
            $data['title']='404';
            $data['slug']='404';
            $page=$data['slug'];
            http_response_code(404);
            $this->view($page,$data);
            return;
        }


        http_response_code(404);
    }

    public function dispatch_post()
    {
        try
        {
            foreach ($this->post as $route => $callable) {
                # code...
                $pattern = '/^' . $route . '$/';
                if(preg_match($pattern, $this->request, $matches)) {
                    array_shift($matches);
                    call_user_func_array($callable, $matches);   
                    return;             
                }
            }
        }
        catch(Exception $exception_error)
        {
            flash('error', 'Some error has occurred. Please try again.');
            $data['flash'] = $_SESSION['flash'];
            unset($_SESSION['flash']);
            $data['title']='404';
            $data['slug']='404';
            $page=$data['slug'];
            http_response_code(404);
            $this->view($page,$data);
            return;
        }

        http_response_code(404);
    }

}