<?php

/**
 * ILogger Interface
 * Defines expected behaviour for logger files...
 * they should able to write a log line.
 * @updated at 2020-09-17
 */

namespace App\Lib;

interface ILogger
{

	public function write($event);

}