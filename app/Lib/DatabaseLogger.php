<?php

namespace App\Lib;
use App\Models\LogModel;

/**
 * @file - DatabaseLogger.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated - 2020-09-18
 */
class DatabaseLogger implements \App\Lib\ILogger
{
    /**
     * [write function as per interface]
     * @param  [string] $event [the details knitted together to enter into log table]
     */
    public function write($event)
    {
        $log_model = new LogModel();
        $log_id = $log_model->save_log($event);
    }
}