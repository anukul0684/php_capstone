<?php

namespace App\Lib;

/**
 * @file - FileLogger.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated - 2020-09-18
 */
class FileLogger implements \App\Lib\ILogger
{
    /**
     * [write function as per interface]
     * @param  [string] $event [the details knitted together to enter into log file]
     */
    public function write($event)
    {
        $myfile = fopen("../logs/events.log", "a") or die("Unable to open file!");
        $txt = $event . "\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }
}