<?php

/**
 * @file - route.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated on - 2020-09-30
 */

use App\Controllers\PagesController as Pages;
use App\Controllers\ServicesController as Services;
use App\Controllers\UsersController as Users;
use App\Controllers\LoginController as Login;
use App\Controllers\AppointmentController as Appointments;
use App\Controllers\AdminController as Admin;

/* Route to About Us page using GET request*/
$route->get('about',function(){    
    $controller = new Pages();
    $controller->about();
});

/* Route to Contact Us page using GET request*/
$route->get('contactus',function(){    
    $controller = new Pages();
    $controller->contactus();
});

/* Route to Courses page using GET request*/
$route->get('courses',function(){    
    $controller = new Pages();
    $controller->courses();
});

/* Route to Products page using GET request*/
$route->get('products',function(){    
    $controller = new Pages();
    $controller->products();
});

/* Route to Seminars page using GET request*/
$route->get('seminars',function(){    
    $controller = new Pages();
    $controller->seminars();
});

/* Route to Services page using GET request*/
$route->get('services',function(){    
    $controller = new Services();
    $controller->services();
});

/* Route to Service Details page using GET request*/
$route->get('service_details',function(){
    $controller = new Services();
    $controller->serviceDetails();
});

/* Route to Services page using POST request when user clicks Clear Cart*/
$route->post('services',function(){
    $controller = new Services();
    $controller->clearCart();
});

/* Route to Appointment Cart page when user adds a service using POST request*/
$route->post('appointment_cart',function(){
    $controller = new Services();
    $controller->addBufferCart();
});

/* Route to Appointment Cart page using GET request when user clicks cart image in header*/
$route->get('appointment_cart',function(){
    $controller = new Services();
    $controller->goToCart();
});

/* Route to Checkout page using GET request when user clicks Proceed to Pay*/
$route->get('checkout_page',function(){
    $controller = new Appointments();
    $controller->index();
});

/* Route to Thank you page using POST request when user pays with card details*/
$route->post('appointment_placed',function(){
    $controller = new Appointments();
    $controller->saveAppointment();
});

/* Route to Appointment Placed page using GET request*/
$route->get('appointment_placed',function(){
    $controller = new Appointments();
    $controller->checkLoggedIn();
});

/* Route to User Appointments Histroy page using GET request*/
$route->get('appointment_history',function(){
    $controller = new Appointments();
    $controller->appointmentIndex();
});

/* Route to Appointment's Service History page using GET request*/
$route->get('service_history',function(){
    $controller = new Appointments();
    $controller->serviceIndex();
});

/* Route to Services At Home page using GET request*/
$route->get('servicesathome',function(){    
    $controller = new Pages();
    $controller->servicesathome();
});

/* Route to Specials page using GET request*/
$route->get('specials',function(){    
    $controller = new Pages();
    $controller->specials();
});

/* Route to Home page using GET request*/
$route->get('/',function(){    
    $controller = new Pages();
    $controller->home();
});

/* Route to Sign Up page using GET request*/
$route->get('user_registration',function(){    
    $controller = new Users();
    $controller->index();
});

/* Route to saving user page using POST request*/
$route->post('user_add',function(){
    $controller = new Users();
    $controller->register();
});

/* Route to Profile page using GET request*/
$route->get('user_success', function(){
    $controller = new Users();
    $controller->login_details();
});

/* Route to Login page using GET request*/
$route->get('login',function(){    
    $controller = new Login();
    $controller->login();
});

/* Route to Login page using POST request*/
$route->post('login',function(){
    $controller = new Login();
    $controller->authenticate();
});

/* Route to logout page using GET request*/
$route->get('logout',function(){
    $controller = new Users();
    $controller->logout();
});

/* Route to Admin Dashboard page using GET request*/
$route->get('dashboard',function(){
    $controller = new Admin();
    $controller->index();
});

/* Route to Admin Users List View page using GET request*/
$route->get('admin_users',function(){
    $controller = new Admin();
    $controller->usersList();
});

/* Route to Admin User Details View page using GET request*/
$route->get('admin_usersdetails',function(){
    $controller = new Admin();
    $controller->userdetailsList();
});

/* Route to Admin Services List View page using GET request*/
$route->get('admin_services',function(){
    $controller = new Admin();
    $controller->servicesList();
});

/* Route to Admin Add Service page using GET request*/
$route->get('admin_serviceAdd',function(){
    $controller = new Admin();
    $controller->addService();
});

/* Route to Admin Add Service page using POST request*/
$route->post('admin_serviceAdd',function(){
    $controller = new Admin();
    $controller->saveService();
});

/* Route to Admin Edit Service page using GET request*/
$route->get('admin_serviceEdit',function(){
    $controller = new Services();
    $controller->serviceDetails();
});

/* Route to Admin Edit Service page using POST request*/
$route->post('admin_serviceEdit',function(){
    $controller = new Admin();
    $controller->serviceUpdate();
});

/* Route to Admin Delete Service page using GET request*/
$route->get('admin_serviceDelete',function(){
    $controller = new Services();
    $controller->serviceDetails();
});

/* Route to Admin Delete Service page using POST request*/
$route->post('admin_serviceDelete',function(){
    $controller = new Admin();
    $controller->serviceDelete();
});

/* Route to Admin Courses page using GET request*/
$route->get('admin_courses',function(){
    $controller = new Admin();
    $controller->coursesList();
});

/* Route to Admin Seminars page using GET request*/
$route->get('admin_seminars',function(){
    $controller = new Admin();
    $controller->seminarsList();
});

/* Route to Admin Products page using GET request*/
$route->get('admin_products',function(){
    $controller = new Admin();
    $controller->productsList();
});

/* Route to Error page in case of Customer and to Dashboard in case of Admin using GET request*/
$allowed_pages = array(
    '/dashboard',
    '/admin_users',
    '/admin_services',
    '/admin_serviceEdit',
    '/admin_serviceDelete',
    '/admin_serviceAdd',
    '/admin_courses',
    '/admin_seminars',
    '/admin_products',
    '/about',
    '/contactus',
    '/courses',
    '/products',
    '/seminars',
    '/services',
    '/service_details',
    '/appointment_cart',
    '/checkout_page',
    '/appointment_placed',
    '/appointment_history',
    '/service_history',
    '/servicesathome',
    '/specials',
    '/user_add',
    '/user_registration',
    '/user_success',
    '/login',
    '/logout',
    '/'
);

if(!(in_array($_SERVER['REQUEST_URI'],$allowed_pages))) {
    $route->get($_SERVER['REQUEST_URI'],function(){
        $controller = new Pages();
        $controller->error404();
    });

}

