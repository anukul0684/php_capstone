<?php
/**
 * @file - settings.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated on - 2020-09-18
 */

return array(
    'DB_FILE' => realpath(__DIR__ . '/../Storage/capstone.sqlite'),
    'SITE_NAME' => 'Angel',
    'REQUEST_URI' => $_SERVER['REQUEST_URI'],
    'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
    'VIEW_PATH' => realpath(__DIR__ . '/../Views'),
    'BASE_PATH' => realpath(__DIR__ . '/../..'),
    'ROUTES_FILE' => realpath(__DIR__ . '/route.php')
);