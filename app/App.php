<?php

namespace App;

use App\Lib\Router;
use App\Lib\Controller;
use App\Models\Model;
use App\Lib\FileLogger;
use App\Lib\DatabaseLogger;

/**
 * @file - App.php
 * @author  - Anu Kulshrestha <[<email address>]>
 * @updated on - 2020-09-18
 */
class App
{
    private $c;
    private $route;
    private $SITE_NAME;
    
    public function __construct(\Pimple\Container $c)
    {
        $this->c = $c;

        Controller::init($c['VIEW_PATH']);

        Model::init($c['dbh']);

        $req = explode('?',$c['REQUEST_URI']);

        $this->route = new Router($req[0], $c['REQUEST_METHOD']);

        $this->loadRoutes($this->route);
        
        $this->SITE_NAME = $c['SITE_NAME'];

        
    }

    public function loadRoutes($route)
    {
        require $this->c['ROUTES_FILE'];
    }

    public function run()
    {
        $this->route->dispatch();
        $file_log = new FileLogger();
        log_function($file_log);
        $db_log = new DatabaseLogger();
        log_function($db_log);
    }


}