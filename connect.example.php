<?php
/**
 * @file-connect.php
 * @author  Anu Kulshrestha <[<kulshrestha-a@webmail.uwinnipeg.ca>]>
 * @updated_at 2020-09-03
 */

//DB credentials
define('DB_DSN', 'mysql:hostname=localhost;dbname=DBNAME');
define('DB_NAME', 'DBNAME');
define('DB_USER','USERNAME');
define('DB_PASS', 'PASSWORD');

//$conn or $dbh an object variable
$dbh = new PDO(DB_DSN,DB_USER,DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);