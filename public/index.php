<?php

/**
 * @file index.php
 * @author Anu Kulshrestha <[<kulshrestha-a@webmail.uwinnipeg.ca>]>
 * updated on 2020-09-18 
 *
 * ****************************************************
 * *      Program Name: Web Development               *
 * *       Course Name: OO PHP                        *
 * *             Batch: WDD7 - Jan 2020               *
 * *        Instructor: Steve George                  *
 * *        Assignment: 2                             *
 * *                                                  *
 * *      Submitted by: Anu Kulshrestha               *
 * ****************************************************
*/


// Always load the autoload.php
// Load before any classes are used
require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../app/Lib/helpers.php';

require __DIR__ . '/../connect.php';


//enabling error display on controller files
ini_set('display_errors',1);
ini_set('error_reporting',E_ALL);

ob_start(); //enable output buffering to avoid innocent or unknowing spaces
session_start(); //start session

// code from stackoverflow.com/questions/6287903/how-to-properly-add-cross-site-request-forgery-csrf-token-using-php
if (!isset($_SESSION['token'])) {
    $token = bin2hex(random_bytes(32));
    $_SESSION['token'] = $token;
    $_SESSION['token_time'] = time();
}
else
{
    $token = $_SESSION['token'];
}

$errors = $_SESSION['errors'] ?? [];

$post = $_SESSION['post'] ?? [];

$flash = $_SESSION['flash'] ?? [];


//max page numbers
define('MAXRESULTSPERPAGE',8);

// Inversion of Control / Dependency Injection
$settings = require __DIR__ . '/../app/Config/settings.php';

// Instantiate the IOC or DI container
$c = new Pimple\Container($settings);

// Add $dbh to container
$c['dbh'] = function () use($c) {    
    $dbh = new PDO(DB_DSN,DB_USER,DB_PASS);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
};

define('SITE_NAME',$c['SITE_NAME']);

$app = new App\App($c);

// May want to do other stuff in here.
$app->run();

unset($_SESSION['errors']);
unset($_SESSION['post']);
unset($_SESSION['flash']);
// dump($app);